(function(APP) {

	APP.contacts = [
		{
			"name": "John",
			"surname": "Doe",
			"email": "john@mail.com",
			"phone": "+380932222222"
		},
		{
			"name": "Luis",
			"surname": "Carrol",
			"email": "luis@mail.com",
			"phone": "+380931111111"
		},
		{
			"name": "John",
			"surname": "Doe",
			"email": "john@mail.com",
			"phone": "+380932222222"
		},
		{
			"name": "Luis",
			"surname": "Carrol",
			"email": "luis@mail.com",
			"phone": "+380931111111"
		},
		{
			"name": "John",
			"surname": "Doe",
			"email": "john@mail.com",
			"phone": "+380932222222"
		},
		{
			"name": "Luis",
			"surname": "Carrol",
			"email": "luis@mail.com",
			"phone": "+380931111111"
		},
		{
			"name": "John",
			"surname": "Doe",
			"email": "john@mail.com",
			"phone": "+380932222222"
		},
		{
			"name": "Luis",
			"surname": "Carrol",
			"email": "luis@mail.com",
			"phone": "+380931111111"
		},
		{
			"name": "Luis",
			"surname": "Carrol",
			"email": "luis@mail.com",
			"phone": "+380931111111"
		},
		{
			"name": "John",
			"surname": "Doe",
			"email": "john@mail.com",
			"phone": "+380932222222"
		},
		{
			"name": "Luis First",
			"surname": "Carrol",
			"email": "luis@mail.com",
			"phone": "+380931234567"
		}
	]

})(window.APP || {});