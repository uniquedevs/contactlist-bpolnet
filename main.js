(function(APP){

    const re_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,

        re_phone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im,

        isValid = element => {
            if ( !element.name || !element.value ) return false;
            if( element.name == 'email' ) return re_email.test(element.value);
            if( element.name == 'phone' ) return re_phone.test(element.value);
            return true;
        },

        formToJSON = elements => [].reduce.call(elements, (data, element) => {
            if( isValid(element) ) {
                data[element.name] = element.value;
            } else {
                if( element.type != 'submit' )
                    (data.invalid = data.invalid ? data.invalid : []).push(element);
            }
            return data;
        }, {})
    ;

    let db,

        initDB = () => {

            let indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;

            return new Promise( resolve => {
                let open = indexedDB.open("ContactsDatabase", 1);

                open.onupgradeneeded = function() {
                    let db = open.result;
                    db.createObjectStore("ContactsObjectStore", {keyPath: "id", autoIncrement:true});
                };

                open.onsuccess = function() {
                    // Start a new transaction
                    db = new DB( open.result );
                    db.count().then( response => {
						resolve(response);
                    });
                };
            });
        },
        // fills db if empty
        fillDB = count => {

            if (!count) {
                console.log('fill db');
                let list = APP.contacts,
                    requests = []
                ;
                list.forEach( contact => {
					requests.push( db.insert( contact ) );
                });

				Promise.all(requests).then( () => {
				    return true;
                });

            } else {
                return Promise.resolve();
            }
        }
    ;

    class Application {
        constructor(){
            this.ui().events();
        }
        ui(){
            this.form = document.querySelector('.js_add_contact_form');
            this.list = document.querySelector('.js_contact_list');
            return this;
        }
        events(){
            // on add contact form submit
            this.form.addEventListener('submit', this.onSubmitForm.bind(this) );

            // remove error validation indicators
            Array.from(this.form.elements).forEach( (el, inx, arr) => {
                el.addEventListener('focus', ()=> {
                    arr.forEach( el => el.classList.remove('no-valid'));
                });
            });

			APP.pubSub.subscribe('page:update', page => {
				page = Number.isInteger(+page) ? page : null;
                this.updatePage(page);
			});
            return this;
        }
        onSubmitForm(e) {
            e.preventDefault();
            const data = formToJSON( this.form.elements );

            // add contact form validation
            if( data.invalid ) {
                data.invalid.forEach( el => el.classList.add('no-valid'));
            } else {
                let contact = new Contact(data);
                contact.save(data);
            }
        }
        start() {
            this.contacts = new Collection();
            this.updatePage(0);
        }
		updatePage( page ){
			this.contacts.fetch().then( list => {
				if (page === null) {
					let current = this.contacts.getCurrentPage(),
						last_page = Math.max(0, Math.floor( (list.length - 1) / 10));
					page = Math.min(current, last_page);
				}
				this.appendList( page );
			} );
        }
        appendList( page ){
			APP.pubSub.publish('contacts:remove');
			this.list.innerHTML = null;
			this.contacts.setCurrentPage( page );
			this.list.append( this.contacts.render().el );
            APP.pubSub.publish('contacts:append');
        }
    }

    class Collection {
        constructor(){
            this.list =[];
            this.pages = [];
            this.current_page = 0;
            this.el = document.createElement('div');
            this.list_el = document.createElement('div');
			this.list_el.classList.add('contact-list-container');
            this.pagin_el = document.createElement('div');
			this.pagin_el.classList.add('pagination-container');
            this.onPageLinkClickBind = this.onPageLinkClick.bind(this);
            this.events();
        }
        setCurrentPage(page) {
			this.current_page = page;
			return this;
        }
        getCurrentPage(){
        	return this.current_page;
		}
        events(){
        	let current_page = this.current_page;
            APP.pubSub.subscribe('contacts:append', () => {
                if(!this.pages.length || !this.pages[this.current_page].length) return;
				this.pages[this.current_page].forEach( contact => {
                    contact.events();
                });

				this.addPaginEvents();
            });
			APP.pubSub.subscribe('contacts:remove', () => {
				if(!this.pages.length || !this.pages[this.current_page].length) return;
				this.pages[this.current_page].forEach( contact => {
					contact.events();
				});

				this.removePaginEvents();
			});
        }
        fetch(){
            return new Promise( resolve => {
            	this.list = [];
                db.getAll().then( list => {
                    list.forEach( contact => {
                        this.list.unshift(new Contact(contact));
                    });
                    resolve(list);
                })
            });
        }

        render(){

            let current_page = this.current_page,
                list = this.list,
                i,j,
                chunk = 10
            ;

            this.pages = [];

			for ( i=0, j=list.length; i<j; i+=chunk ) {
				this.pages.push(list.slice(i,i+chunk));
			}

			this.list_el.innerHTML = null;

			if(this.pages.length) {
				this.pages[current_page].forEach( contact => {
					this.list_el.append(contact.render().el);
				});
			}

            this.el.append( this.list_el );

			this.renderPagin();

            return this;
        }

        renderPagin(){
			this.pagin_el.innerHTML = null;

			this.pages.forEach( (page, index) => {
			    let is_current = index == this.current_page ? 'active' : '';
				this.pagin_el.innerHTML += `<a href="" data-page="${index}" class="${is_current}">${index}</a>`;
            });

			this.el.append( this.pagin_el );
        }

		onPageLinkClick(event){
			event.preventDefault();
			APP.pubSub.publish('page:update', event.target.dataset.page);
        }

        addPaginEvents(){
			const pageLinks = this.pagin_el.querySelectorAll('a');

			Array.from(pageLinks).forEach(el => {
				el.addEventListener('click', this.onPageLinkClickBind );
			});
			return this;
        }

		removePaginEvents(){
			const pageLinks = this.pagin_el.querySelectorAll('a');

			Array.from(pageLinks).forEach(el => {
				el.removeEventListener('click', this.onPageLinkClickBind);
			});
			return this;
        }
    }

    class Contact {
        constructor( info ){
			for (let i in info) {
                this[i] = info[i];
            }
            this.el = document.createElement('div');
            this.el.classList.add('contact-item');
            this.onDeleteClickBind = this.onDeleteClick.bind(this);
            this.onEditeClickBind = this.onEditeClick.bind(this);
            this.onInputClickBind = this.onInputClick.bind(this);
        }
        events() {
            const inputElements = this.el.querySelectorAll('input'),
					deleteBtn = this.el.querySelector('.js_delete'),
					editBtn = this.el.querySelector('.js_edit')
			;

            Array.from(inputElements).forEach(el => {
                el.addEventListener('change', this.onInputClickBind );
            });

			deleteBtn.addEventListener( 'click', this.onDeleteClickBind );
			editBtn.addEventListener( 'click', this.onEditeClickBind );
            return this;
        }
        onDeleteClick(event){
        	event.preventDefault();
        	this.deleteSelf(this.id);
		}
		onEditeClick(event){
			event.preventDefault();
			const elements = this.el.querySelectorAll('input'),
				data = formToJSON( elements );
			data.id = this.id;

			//contact validation
			if( data.invalid ) {
				data.invalid.forEach( el => el.classList.add('no-valid'));
			} else {
				this.update(data);
			}
		}
        onInputClick(event){
			event.preventDefault();
			if(this[event.target.name] !== event.target.value) {
				event.target.classList.add('modified');
			}
        }
        removeEvents(){
			const inputElements = this.el.querySelectorAll('input'),
					deleteBtn = this.el.querySelector('.js_delete'),
					editBtn = this.el.querySelector('.js_edit');

			Array.from(inputElements).forEach(el => {
				el.removeEventListener('change',this.onInputClickBind);
			});

			deleteBtn.removeEventListener( 'click', this.onDeleteClickBind);
            editBtn.removeEventListener( 'click', this.onEditeClickBind);
			return this;
        }
        render() {
            this.el.innerHTML = null;
            this.el.innerHTML += `
                <div class="field">
                    <input type="text" name="name" value="${this.name}">
                </div>
                <div class="field">
                    <input type="text" name="surname" value="${this.surname}">
                </div>
                <div class="field">
                    <input type="text" name="email" value="${this.email}">
                </div>
                <div class="field">
                    <input type="text" name="phone" value="${this.phone}">
                </div>
                <div class="field">
                    <a href="#" class="edit-item js_edit">Edit</a>
                </div>
                <div class="field">
                    <a href="#" class="delete-item js_delete">Delete</a>
                </div>
            `;
            return this;
        }
        save(data){
			db.insert(data).then( () => {
				APP.pubSub.publish('page:update');
			});
		}
		update(data){
			db.insert(data).then( () => {
				APP.pubSub.publish('page:update');
			});
		}
		deleteSelf(id){
			db.deleteItem(id).then( () => {
				APP.pubSub.publish('page:update');
			});
		}
    }

    class DB {
        constructor( idb ){
            this.db = idb;
        }
        getObjectStore(){
            let tx = this.db.transaction("ContactsObjectStore", "readwrite");
            return tx.objectStore("ContactsObjectStore");
        }
        insert(obj) {
            return new Promise( resolve => {
                let
                    store = this.getObjectStore()
                ;

                let request = store.put(obj);

                request.onsuccess = () =>  {
                    resolve();
                }
            })
        }
        getAll() {
            return new Promise( resolve => {
                let
                    store = this.getObjectStore(),
                    request = store.openCursor(),
                    cursor,
                    data = []
                    ;

                request.onsuccess = function () {
                    cursor = request.result;
                    if (cursor) {
                        data.push(cursor.value);
                        cursor.continue();
                    } else {
                        resolve(data);
                    }
                };
            });
        }
        get(email){
            return new Promise( resolve => {
                let
                    store = this.getObjectStore(),
                    index = store.index("Email"),
                    request = index.get(email)
                ;

                request.onsuccess = function() {
                    resolve( request.result );
                };
            });
        }
        count(){
            return new Promise( resolve => {
                let
                    store = this.getObjectStore(),
                    count = store.count()
                ;

                count.onsuccess = () => {
                    resolve( count.result );
                }

            })
        }
        deleteItem(id) {
			return new Promise( resolve => {
				let
					store = this.getObjectStore(),
					request = store.delete(id)
				;

				request.onsuccess = () => {
					resolve( id );
				}
			})
		}
    }

    document.addEventListener("DOMContentLoaded", function(event) {
        let app = new Application();
        initDB()
            .then( fillDB )
            .then( app.start.bind(app) );
    });

})(window.APP || {});